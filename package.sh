#!/usr/bin/env bash

# fail on error
set -e

# General package information
NAME="$(/usr/bin/env python setup.py --name)"
VERSION="$(/usr/bin/env python setup.py --version)"
URL="$(/usr/bin/env python setup.py --url)"
LICENSE="$(/usr/bin/env python setup.py --license)"
DESCRIPTION="$(/usr/bin/env python setup.py --description)"
MAINTAINER="$(/usr/bin/env python setup.py --maintainer)"
PREV_BUILD_NUMBER="$(cat ./files/BUILD_NUMBER)"
BUILD_NUMBER="$(($PREV_BUILD_NUMBER + 1))"

# output directory for built packages
DIST_DIR="$(pwd)/rpm"

# cleanup dist directory
rm -rf ./dist 2> /dev/null

BUILD_ROOT="$(pwd)/buildroot"

# cleanup build root
rm -rf "${BUILD_ROOT}" 2> /dev/null

# create build root
mkdir -p "${BUILD_ROOT}"

# create distribution root
mkdir -p "${DIST_DIR}"

# create tar archive using setuptools
/usr/bin/env python setup.py sdist

tar xvzf "./dist/${NAME}-${VERSION}.tar.gz" -C "${BUILD_ROOT}"

RPM_ROOT="${BUILD_ROOT}/rpm"

# where package files to be located
PACKAGE_FILES_DIR="${RPM_ROOT}/opt/${NAME}"
LOCAL_PYTHON="${PACKAGE_FILES_DIR}/bin/python"
virtualenv "${PACKAGE_FILES_DIR}"

"${LOCAL_PYTHON}" "${BUILD_ROOT}/${NAME}-${VERSION}/setup.py" install

# system config location
SYSTEM_CONFIG_DIR="${RPM_ROOT}/etc/${NAME}"
mkdir -p "${SYSTEM_CONFIG_DIR}"

# systemd system-wide units location
SYSTEMD_UNITS_DIR="${RPM_ROOT}/etc/systemd/system"
mkdir -p "${SYSTEMD_UNITS_DIR}"

# nginx config files location
NGINX_CONFIG_DIR="${RPM_ROOT}/etc/nginx/conf.d"
mkdir -p "${NGINX_CONFIG_DIR}"

cp ./files/config.yml "${SYSTEM_CONFIG_DIR}/${NAME}.yml"
cp ./files/boilerplate.uwsgi.ini "${SYSTEM_CONFIG_DIR}"

cp ./files/boilerplate-webapp.service "${SYSTEMD_UNITS_DIR}"
cp ./files/boilerplate-worker.service "${SYSTEMD_UNITS_DIR}"

cp ./files/boilerplate.conf "${NGINX_CONFIG_DIR}"

fpm -s dir -t rpm \
    --maintainer "${MAINTAINER}" \
    --iteration "${BUILD_NUMBER}" \
    --name "${NAME}" \
    --epoch "$(date +%s)" \
    --vendor Denis \
    --version "${VERSION}" \
    --license "${LICENSE}" \
    --depends "nginx" \
    --category "Applications/Inventory" \
    --url "${URL}" \
    --description "${DESCRIPTION}" \
    --rpm-os linux \
    --architecture x86_64 \
    --config-files "etc/${NAME}/${NAME}.yml" \
    --config-files "etc/systemd/system/${NAME}-webapp.service" \
    --config-files "etc/systemd/system/${NAME}-worker.service" \
    --config-files "etc/nginx/conf.d/${NAME}.conf" \
    --chdir "${RPM_ROOT}"

# raise iteration
echo "${BUILD_NUMBER}" > ./files/BUILD_NUMBER

# clean up
rm -rf "${BUILD_ROOT}"

# fpm build dir
rm -rf ./build/

# python dist dir
rm -rf ./dist/

RPM_PACKAGE_NAME="${NAME}-${VERSION}-${BUILD_NUMBER}.x86_64.rpm"

# move rpm packages
mv *.rpm "${DIST_DIR}"
