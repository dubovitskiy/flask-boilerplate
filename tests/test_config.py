import tempfile
import os

import yaml

from boilerplate.config import parse_config


def test_config_use_defaults():
    # non-existing file
    conf = parse_config('/blahblah')
    assert conf['webapp_port'] == 5000


def test_config_file_overrides_params():
    config = parse_config('/non-existing-file')
    # just to be sure
    assert config['webapp_port'] == 5000

    # fulfilling test config file
    _, config_filename = tempfile.mkstemp()
    settings = {'webapp_port': 8080}
    with open(config_filename, 'w') as config_file:
        yaml.dump(settings, config_file)

    conf = parse_config(config_filename)
    assert conf['webapp_port'] == 8080
    assert conf['webapp_host'] == 'localhost'

    try:
        os.unlink(config_filename)
    except OSError:
        pass
