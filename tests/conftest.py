import time

import docker
import pytest
from sqlalchemy.engine import url

from boilerplate.models.base import Base, get_session, get_engine, session
from boilerplate.webapp.application import create_app

LOCAL_POSTGRES_PORT = 5433


def postgres_container():
    docker_client = docker.from_env()
    print('Starting new postgresql container')
    container = docker_client.containers.run(
        image='postgres',
        environment=[
            'POSTGRES_PASSWORD=password',
            'POSTGRES_DB=boilerplate',
        ],
        ports={'5432/tcp': LOCAL_POSTGRES_PORT},
        network_mode='bridge',
        remove=True,
        detach=True,
    )
    time.sleep(2)
    return container


postgres_url = url.URL(
    drivername='postgresql',
    username='postgres',
    password='password',
    host='127.0.0.1',
    port=LOCAL_POSTGRES_PORT,
    database='boilerplate'
)


@pytest.yield_fixture(scope='module')
def app():
    container = postgres_container()
    overrides = {
        'database_connection': postgres_url,
        'TESTING': True
    }
    application = create_app(overrides)
    try:
        Base.metadata.create_all(bind=session.bind)
    except:
        container.stop()
    yield application
    session.close()
    container.stop()


@pytest.yield_fixture(scope='module')
def db_session(app):
    """Session to be used during tests."""
    _db_session = get_session()
    _db_session.bind = get_engine(postgres_url)
    return _db_session
