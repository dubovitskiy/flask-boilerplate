import json

from boilerplate.models import Host
from boilerplate.webapp.application import create_app


def test_config_defaults():
    app = create_app()
    assert app.config['webapp_port'] == 5000
    assert app.config['webapp_host'] == 'localhost'


def test_app_config_overrides():
    app = create_app({
        'webapp_port': 8080
    })
    assert app.config['webapp_port'] == 8080


def test_app_hosts_list(app, db_session):
    client = app.test_client()
    response = client.get('/hosts')
    assert response.status_code == 200
    json_response = json.loads(response.data)
    assert len(json_response) == 0

    host = Host(hostname='test')
    db_session.add(host)
    db_session.commit()

    response = client.get('/hosts')
    assert response.status_code == 200
    json_response = json.loads(response.data)
    assert len(json_response) == 1


def test_host_detail(app, db_session):
    client = app.test_client()
    response = client.get('/hosts/100500')
    assert response.status_code == 404

    host = Host(hostname='hostname')
    db_session.add(host)
    db_session.commit()
    response = client.get('/hosts/%i' % host.id)
    assert response.status_code == 200
