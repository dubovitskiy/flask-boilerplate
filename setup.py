from setuptools import setup, find_packages

setup(
    name='boilerplate',
    version='1.0.0',
    packages=find_packages(),
    url='http://test.com',
    license='MIT',
    author='test',
    author_email='test@test.com',
    description='Test',
    install_requires=[
        'flask',
        'PyYaml',
        'uwsgi',
        'celery'
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
