from celery import Celery

from boilerplate import config
from boilerplate.constants import DEFAULT_CONFIG_LOCATION


def create_app():
    celery_app = Celery()
    app_config = config.parse_config(DEFAULT_CONFIG_LOCATION)
    celery_app.config_from_object(app_config, namespace='worker')
    return celery_app


app = create_app()
