from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


def get_engine(connection_string):
    engine = create_engine(
        connection_string,
        convert_unicode=True,
        pool_recycle=3600,
        pool_size=10,
    )
    return engine


def get_session():
    session_class = sessionmaker(
        autocommit=False,
        autoflush=False,
    )

    return scoped_session(session_class)


session = get_session()

Base = declarative_base()
