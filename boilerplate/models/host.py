import sqlalchemy as sa

from boilerplate.models.base import Base


class Host(Base):
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    hostname = sa.Column(sa.String(64), nullable=False)

    __tablename__ = 'hosts'
