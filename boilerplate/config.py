import copy
import logging

import yaml

DEFAULTS = {
    # Developer settings
    'DEBUG': False,
    'TESTING': False,

    # Web application settings
    'webapp_port': 5000,
    'webapp_host': 'localhost',

    # Celery worker settings
    'worker_broker_url': 'amqp://guest:guest@localhost:5672//',

    # PostgreSQL database connection
    'database_connection': 'postgresql://postgres:password@localhost:5432/boilerplate',
}

logger = logging.getLogger('config')
logger.addHandler(logging.StreamHandler())


def parse_config(config_path):
    """Parse application config file."""
    config = copy.copy(DEFAULTS)
    overrides = {}

    try:
        with open(config_path, 'r') as config_file:
            overrides = yaml.load(config_file)
    except IOError:
        logger.warn('Config file is not found. Using defaults.')

    if not isinstance(overrides, dict):
        logger.error('Config file is invalid. Using defaults.')
        overrides = {}

    config.update(overrides)
    return config
