#!/usr/bin/env/python
from argparse import ArgumentParser

from boilerplate.webapp.application import create_app
from boilerplate.constants import DEFAULT_CONFIG_LOCATION


parser = ArgumentParser()

parser.add_argument(
    '-c', '--config',
    dest='config_filename',
    default=DEFAULT_CONFIG_LOCATION,
    help='Config file'
)

args = parser.parse_args()
app = create_app(config_filename=args.config_filename)


if __name__ == '__main__':
    app.run(
        host=app.config['webapp_host'],
        port=app.config['webapp_port'],
        debug=app.config['DEBUG'],
    )
