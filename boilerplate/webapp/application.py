from flask import Flask

from boilerplate import config
from boilerplate.constants import DEFAULT_CONFIG_LOCATION
from boilerplate.models.base import session, get_engine
from boilerplate.webapp.hosts import hosts_bp


def create_app(config_overrides=None,
               config_filename=DEFAULT_CONFIG_LOCATION):
    # type: (dict, dict) -> Flask
    app = Flask(__name__)

    if not config_overrides:
        config_overrides = {}

    app_config = config.parse_config(config_filename)
    app_config.update(config_overrides)
    app.config.update(app_config)

    # Setting engine for the session
    session.bind = get_engine(app_config['database_connection'])

    app.register_blueprint(hosts_bp)
    return app
