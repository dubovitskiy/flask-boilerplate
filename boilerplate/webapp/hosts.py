from flask import Blueprint, jsonify

from boilerplate.models import Host
from boilerplate.models.base import session

hosts_bp = Blueprint('hosts', __name__)


@hosts_bp.route('/hosts')
def hosts_list():
    hosts = session.query(Host).all()
    return jsonify([
        {'id': host.id, 'hostname': host.hostname}
        for host in hosts
    ])


@hosts_bp.route('/hosts/<int:host_id>')
def host_detail(host_id):
    host = session.query(Host).get(host_id)
    if not host:
        return jsonify({}), 404
    return jsonify({'id': host.id, 'hostname': host.hostname})
