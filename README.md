Run project

```bash
$ pip install pipenv
$ pipenv install
$ pipenv shell
$ python boilerplate/webapp/wsgi.py
```

Tests:

Ensure docker daemon is running and your user has rights on starting containers.
You can add your user to the `docker` group: `usermod -aG docker <username>`.

```bash
$ PYTHONPATH=. python setup.py test
```

Lint:

```bash
$ python setup.py lint
```

Run dev containers
```bash
$ docker-compose up -d
```
